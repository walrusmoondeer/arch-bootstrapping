#!/usr/bin/env bash

# TODO: /home directory should have its own partition?
# * in the case of ext4 encryption, it is most useful to encrypt only /home directory?
# * in the case of btrfs raid, it is most useful to mirror only the /home directory?
# TODO: full-disk-encryption option 
# TODO: secure boot 
# TODO: why installer doesnt work on usb, maybe usb booting works only with grub???

# VARIABLES (configure these before running the script):
kernal="linux"
localtime="Europe/Riga"
keymap="lv"
lang="en_US.UTF-8"
user="supreme_user"
guest_passwd="1234"
font_list=(noto-fonts noto-fonts-emoji ttf-jetbrains-mono ttf-droid ttf-mononoki-nerd)
base_devel_without_sudo=(archlinux-keyring autoconf automake binutils bison debugedit fakeroot file findutils flex gawk gcc gettext grep groff gzip libtool m4 make pacman patch pkgconf sed texinfo which)
# Colors for colorful prompts/text:
RED='\033[0;31m'
NC='\033[0m' # No Color
#BLUE='\033[0;34m'

# Terminate script if no internet:
ping -c1 google.com || { echo -e "${RED}No internet connection detected, script terminated!${NC}"; exit; }

function choosing_disk {
  lsblk
  read -rp "Enter disk name, example: /dev/sdX  : " disk
  echo -e "\n"
  fdisk -l "${disk}" || { echo -e "${RED}Entered disk name does not exist, script terminated!${NC}"; choosing_disk; }
  read -rp "Are you sure you want to use ${disk} for arch install? [y/N]: " disk_confirmed
  case "${disk_confirmed}" in 
    [Yy]) echo -e "${RED}!!! ${disk} contents will be overwriten!!!${NC}" ;;
    *) echo -e "${RED}No disk selected for installation${NC}"; choosing_disk ;;
  esac }
choosing_disk

# Prompt for 'portable', portable devices need additional graphics drivers, file system drivers, different grub settings, etc...
read -rp "Installing system on a portable device like a usb drive [y/N]?: " portable

# Prompt for 'hostname'
read -rp "Enter desired device name, AKA hostname: " hostname

# If 'user' is not specified above, then prompt for 'user'
if [ -z "${user}" ]; then 
  read -rp "Please enter desired user_name: " user
fi

# Setting up Graphics drivers:
PS3="Choose desired GPU driver collection: "
options=("amd" "intel" "nvidia" "nvidia-opensource" "ati" "all")
select opt in "${options[@]}"; do
  case $opt in 
    "amd") 
      graphics_drivers=(mesa xf86-video-amdgpu vulkan-radeon)
      break ;;
    "intel") 
      graphics_drivers=(mesa xf86-video-intel)
      break ;;
    "nvidia") 
      graphics_drivers=(nvidia nvidia-utils)
      break ;;
    "nvidia-opensource") 
      graphics_drivers=(mesa xf86-video-nouveau)
      break ;;
    "ati") 
      graphics_drivers=(mesa xf86-video-ati vulkan-radeon)
      break ;;
    *)
      echo -e "${RED}Installing all drivers!${NC}"
      graphics_drivers=(mesa xf86-video-amdgpu xf86-video-intel nvidia xf86-video-ati vulkan-radeon nvidia-utils xf86-video-vesa xf86-video-fbdev)
      break ;;
  esac
done

function prompt_for_user_passwd {
echo -e " "
read -srp "Create a password for ${user}: " user_password
echo -e " "
read -srp "Re-enter password for ${user}: " user_password1
if [ "$user_password" = "$user_password1" ]; then
	echo -e "\nSuccess!"
else
  echo -e "${RED}Passwords didn't match, try again!${NC}"
  prompt_for_user_passwd 
fi }
prompt_for_user_passwd 

# Checking Device CPU, for which cpu ucode to use:
if grep -q "AMD" /proc/cpuinfo; then
  cpu="amd"
elif grep -q "Intel" /proc/cpuinfo; then
  cpu="intel"
else 
  echo -e "${RED}Script can't identify cpu vendor!${NC}"; exit
fi
ucode="${cpu}-ucode"

# If disk name ends with a number, then partitions have a 'p' prefix. applies to nvmes and sd-cards.
if [[ "${disk:((${#disk}-1)):1}" =~ [0-9] ]]; then
  bootpart="${disk}p1"
  rootpart="${disk}p2"
else
  bootpart="${disk}1"
  rootpart="${disk}2"
fi

# Checking if the system supports UEFI
if [ -d /sys/firmware/efi/efivars ]; then uefi=1; else uefi=0; fi

# Formating the partitions:
# add '-s' flag to parted scripts to make them not prompt for questions.
if [ ${uefi} != 1 ]; then 
	parted -s -a optimal "${disk}" -- mklabel mbr \
    	mkpart primary ext4 1MB 100% \
		set 1 boot on
else
	parted -s -a optimal "${disk}" -- mklabel gpt \
    	mkpart ESP fat32 1MB 513MB \
		set 1 boot on \
		mkpart primary ext4 513MB 100%
fi 

mkfs.fat -F32 "${bootpart}"
mkfs.ext4 "${rootpart}"

# Mounting:
mount "${rootpart}" /mnt
mkdir /mnt/boot
mount "${bootpart}" /mnt/boot

# Locks Root user, nobody can login as Root.
passwd -l root 

# Populating arch keyring:
pacman --noconfirm --needed -Sy
pacman --noconfirm --needed -S archlinux-keyring
pacman-key --populate archlinux
# Installing Base packages:
pacstrap /mnt base ${kernal} linux-firmware neovim opendoas networkmanager dhcpcd ${ucode} git apparmor man-db "${base_devel_without_sudo[@]}" || { echo -e "${RED}An error occured when installing base packages!${NC}"; exit; }

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt ln -sf /usr/share/zoneinfo/${localtime} /etc/localtime
timedatectl set-ntp true
hwclock --systohc
# By default Linux always changes time stored on Motherboard as UTC, windows stores local time on Motherboard.
# Useful for Dual-boot setups.
timedatectl set-local-rtc 1 --adjust-system-clock 

sed -i '/#en_US.UTF-8 UTF-8/s/^#//g' /mnt/etc/locale.gen
sed -i '/#en_US ISO-8859-1/s/^#//g' /mnt/etc/locale.gen
sed -i '/#lv_LV.UTF-8 UTF-8/s/^#//g' /mnt/etc/locale.gen
sed -i '/#lv_LV ISO-8859-13/s/^#//g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen

echo "LANG=${lang}" >> /mnt/etc/locale.conf
echo "${hostname}" >> /mnt/etc/hostname
echo "KEYMAP=${keymap}" >> /mnt/etc/vconsole.conf

echo -e "127.0.0.1\tlocalhost
::1\t\t\tlocalhost
127.0.1.1\t${hostname}.localdomain\t${hostname}" > /mnt/etc/hosts

# Installing boot loader:
if [[ "${portable}" =~ [Yy] ]]; then # Installing Grub for portable storage device
  # pacstrap /mnt dosfstools ntfs-3g intel-ucode amd-ucode ;;
  # https://wiki.archlinux.org/title/Install_Arch_Linux_on_a_removable_medium
	arch-chroot /mnt pacman --noconfirm --needed -S grub 
	arch-chroot /mnt grub-install --target=i386-pc "${bootpart}" --recheck
  # arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=esp --removable --recheck
  # Enabling Apparmor and adding boot options to increase security.
  sed -i 's/#write-cache/write-cache/g' /mnt/etc/apparmor/parser.conf
  sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=/GRUB_CMDLINE_LINUX_DEFAULT="nomodeset apparmor=1 security=apparmor slab_nomerge slub_debug=FZ init_on_alloc=1 init_on_free=1 mce=0 pti=on mds=full,nosmt module.sig_enforce=1 oops=panic"/g' /mnt/etc/default/grub
	arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg 
elif [ ${uefi} = 1 ]; then # Install Systemd-boot and Apparmor:
	arch-chroot /mnt bootctl install

	echo "timeout 0
	editor no
	default arch.conf" > /mnt/boot/loader/loader.conf

	echo -e "title\tarch-${kernal}
	linux\t/vmlinuz-${kernal}
	initrd\t/${ucode}.img
	initrd\t/initramfs-${kernal}.img
	options\troot=${rootpart} rw apparmor=1 lsm=lockdown,yama,apparmor" >> /mnt/boot/loader/entries/arch.conf
else # Install Grub and Apparmor:
	arch-chroot /mnt pacman --noconfirm --needed -S grub 
	arch-chroot /mnt grub-install --target=i386-pc "${bootpart}"
  # Enabling Apparmor and adding boot options to increase security.
  sed -i 's/#write-cache/write-cache/g' /mnt/etc/apparmor/parser.conf
  sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=/GRUB_CMDLINE_LINUX_DEFAULT="apparmor=1 security=apparmor slab_nomerge slub_debug=FZ init_on_alloc=1 init_on_free=1 mce=0 pti=on mds=full,nosmt module.sig_enforce=1 oops=panic"/g' /mnt/etc/default/grub
	arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg 
fi 
arch-chroot /mnt systemctl enable apparmor.service

arch-chroot /mnt systemctl enable NetworkManager

# Adding $USER
arch-chroot /mnt useradd -mG wheel,audio,video,storage,network,optical "${user}"

# Adding Guest user (doesnt have any permissions)
arch-chroot /mnt useradd -m "guest"

# Adding my Doas config file:
sed -i "s/supreme_user/${user}/g" "$PWD"/assets/doas.conf
cp "$PWD"/assets/doas.conf /mnt/etc/doas.conf

echo "
[multilib]
Include = /etc/pacman.d/mirrorlist" >> /mnt/etc/pacman.conf

# Syncing pacman:
arch-chroot /mnt pacman -Sy 
arch-chroot /mnt pacman --noconfirm --needed -S archlinux-keyring
arch-chroot /mnt pacman-key --populate archlinux

# Installing Graphics Drivers
arch-chroot /mnt pacman --noconfirm --needed -S "${graphics_drivers[@]}" 
# Installing Font List
arch-chroot /mnt pacman --noconfirm --needed -S "${font_list[@]}"

# Setting $USER password:
echo "${user}:${user_password}" | arch-chroot /mnt chpasswd 

# Setting Guest password:
echo "guest:${guest_passwd}" | arch-chroot /mnt chpasswd

# Hardening default file permission model:
sed -i 's/^umask.*/umask\ 077/' /mnt/etc/profile

# Hardening SSH:
# echo "PermitRootLogin no" >> /mnt/etc/ssh/sshd_config   <=== doent work

# Moving arch-bootstrapping scripts to $HOME directory 
cp -r ../arch-bootstrapping /mnt/home/"${user}"/

# Symlinking Doas to Sudo
ln -s "$(arch-chroot /mnt which doas)" /mnt/usr/bin/sudo

# Make pacman colorful.
grep -q "ILoveCandy" /mnt/etc/pacman.conf || sed -i "/#VerbosePkgLists/a ILoveCandy" /mnt/etc/pacman.conf
sed -i "s/^#Color$/Color/g" /mnt/etc/pacman.conf
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 5/g" /mnt/etc/pacman.conf
