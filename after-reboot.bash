#!/usr/bin/env bash

# Colors for colorful prompts/text:
RED='\033[0;31m'
NC='\033[0m' # No Color
BLUE='\033[0;34m'

# Terminate script if no internet connection:
ping -c1 google.com || { echo -e "${RED}No internet connection detected, script terminated!${NC}"; exit; }

# Check if the entered git repo is clonable.
git ls-remote --heads "$1" || { echo -e "${RED}Specify functioning dotfiles git repo as argument for this script, script terminated!${NC}"; exit; }

# Read lines from backup_paths into the 'lines' array using mapfile
mapfile -t lines < assets/pkg_list
# Declare an empty array to store the filtered lines
extra_packages=()
# Iterate over the 'lines' array and exclude elements starting with '#'
for line in "${lines[@]}"; do
    if [[ ! $line =~ ^# ]]; then
        extra_packages+=("${line// /}")
    fi
done

#  === INSTALLING YAY ===
echo -e "${BLUE}Installing yay: \n${NC}"
git -C "$HOME"/ clone https://aur.archlinux.org/yay.git
chmod 777 "$HOME"/yay
cd "$HOME"/yay/ || { echo -e "${RED}couldnt enter yay directory${NC}"; exit; }
makepkg --noconfirm -si || { echo -e "${RED}couldnt install yay${NC}"; exit; }
#     Deleting yay source files:
echo -e "${BLUE}Removing yay directory from \$HOME: \n${NC}"
doas rm -rf /home/"$USER"/yay || { echo -e "${RED}failed to remove yay source dir${NC}"; exit; }

echo -e "${BLUE}Populating arch keyring: ${NC}"
doas pacman --noconfirm --needed -Sy
doas pacman --noconfirm --needed -S archlinux-keyring
doas pacman-key --populate archlinux 

function installing_extra_packages {
  try_again=""
  proceed=""
  echo -e "${BLUE}Installing: ${extra_packages[*]}${NC}" # print what to download, to see what caused error if any.
  yay --noconfirm --needed -S "${extra_packages[@]}" || read -rp "There was an error when installing packages, do you want to try again? [y/N] " try_again; 
  case ${try_again} in 
    [Yy]) installing_extra_packages ;;
    [Nn]) read -rp "Do you want to proceed with the script [y/N]?" proceed; 
      case ${proceed} in 
        [Nn]) echo -e "${RED}Script terminated!${NC}"; exit ;; 
      esac ;; 
  esac 
}
installing_extra_packages 

# Dealing with dotfiles:
yadm init -f # force, in case it decides to protest for some reason 
yadm branch -m main # Renames default branch from 'master' to 'main'
yadm remote add origin "$1"
yadm pull origin main

# Device specific configs
rm "$HOME"/.config/alacritty/alacritty.toml
if grep -q "[Ll]aptop" /etc/hostname; then 
  ln "$HOME"/.config/alacritty/laptop/alacritty.toml "$HOME"/.config/alacritty/alacritty.toml
elif grep -q "[Dd]esktop" /etc/hostname; then 
  ln "$HOME"/.config/alacritty/desktop/alacritty.toml "$HOME"/.config/alacritty/alacritty.toml
fi

# Minor settings:
chsh -s /usr/bin/zsh # changing default shell for user

# Installing SDDM:
echo -e "${BLUE}Enabling display manager [SDDM]: ${NC}" 
doas systemctl enable sddm.service

# ZSH syntax highlighting
mkdir -p "$HOME"/.config/zsh/scripts/
git -C "$HOME"/.config/zsh/scripts/ clone https://github.com/zsh-users/zsh-syntax-highlighting.git
git -C "$HOME"/.config/zsh/scripts/ clone https://github.com/zsh-users/zsh-autosuggestions.git
git -C "$HOME"/.config/zsh/scripts/ clone https://github.com/hlissner/zsh-autopair.git

# Nietzsche quotes for custom fortune-mod
# (disabling this for now)
#mkdir -p "$HOME"/.config/fortune/
#git -C "$HOME"/.config/fortune/ clone https://github.com/iangreenleaf/nietzsche
