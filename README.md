- **NOTE:** These scripts are limited in functionality and opinionated, use them as a point of reference for creating your own script.

# Repo contents:
- **installer.bash** <-- Installs minimal system, with 'WM' and 'login screen', if requested. 
- **after-reboot.bash** <-- Installs dotfiles, packages from pkg_list and does some minor configuration.
- **/assets** <-- Contains assets for 'after-reboot.bash'
  - **doas.conf** <-- My 'doas' permission file, 'doas' is a minimal 'sudo' replacement.
  - **pkg_list** <-- Contains a list of packages to install.

# Installation Steps:
1. Run **installer.bash** directly from the ISO. After the script finishes, shutdown the system and unplug the arch ISO, as it won't be needed any more.
2. Run **after-reboot.bash** after **installer.bash** is finished and system rebooted, **after-reboot.bash** will be located in users home directory, **after-reboot.bash** can install dotfiles (from specified git repo), and packages (listed in pkg_list). You'll need to pass a link to a git repo as an argument when executing **after-reboot.bash**, like this: 
```bash
./after-reboot.bash https://codeberg.org/kmelb/dotfiles.git 
```
# Connecting to Internet before Installation:
### Connecting to wifi from ISO:
```bash
iwctl
device list
station <wlan0> get-networks
station <wlan0> connect <wifi-network-name>
exit
```
### Connecting to internet using USB-tethering:
```bash
ls /sys/class/net
dhcpcd <en........> # enter the longest string, that starts with en....
```
# Downloading arch-bootstrapping scripts on the ISO:
```bash 
pacman -Sy git
git clone -b testing https://codeberg.org/kmelb/arch-bootstrapping.git # for testing repo
git clone -b artix-testing https://codeberg.org/kmelb/arch-bootstrapping.git # for atrix testing  (install artix-openrc instead of arch)
cd arch-bootstrapping
./installer.bash
```

# Connecting to Wifi after base installation from terminal:
```bash 
nmcli networking on
nmcli dev wifi list 
doas nmcli dev wifi connect <wifi_name> password <wifi_password>
```

# Installing my bubblewrap profiles:
```bash 
cd /usr/local/bin 
git clone https://codeberg.org/kmelb/bubblewrap-profiles.git
sudo setup.bash 
sudo rm setup.bash exportFilter.bin
```
# Installing optional xorg+awesome and their settings:
```bash 
git clone https://codeberg.org/kmelb/xorg-installation.git
# Read the README.md for further instructions 
```
